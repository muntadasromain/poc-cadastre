#!/bin/bash

# Store the URL of the website you want to scrape
url="https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/communes"

dptCount=0

# Use wget to download the website's HTML
wget $url -q -O - | grep -o 'href=".*"' | awk -F '"' '{print $2}' |

# Use while loop to iterate through the links
while read link; do
  # Skip links with innerText "../"
  if [[ $link == "../" ]]; then
    continue
  fi
  
	link=${link%/}

  # Visit the link
  wget $url/$link -q -O - | grep -o 'href=".*"' | awk -F '"' '{print $2}' |

  while read sublink; do
    if [[ $sublink == "../" ]]; then
      continue
    fi
    
	sublink=${sublink%/}

	folder="./src/assets/cadastre/$link/$sublink"
	if [ ! -d "$folder" ]; then
		mkdir -p $folder
	fi

	curl -s $url/$link/$sublink/cadastre-$sublink-parcelles.json.gz | gzip -d > $folder/$sublink-parcelles.json

	progress=$(echo "$dptCount / 101 * 100" | bc -l)

	printf "Terminé %d\n" $progress

	#while read fileLink; do
	#	if [[ $fileLink == "../" ]]; then
	#		continue
	#	fi
	#
	#	if [[ $fileLink == "raw/" ]]; then
	#		continue
	#	fi
#
	#	fileLink=${fileLink%/}
	#	folder="./src/assets/cadastre/$link/$sublink"
#
	#	if [[ "$fileLink" == *"batiment"* ]]; then
	#		fileName="batiments.json"
	#	elif [[ "$fileLink" == *"communes"* ]]; then
	#		fileName="communes.json"
	#	elif [[ "$fileLink" == *"feuilles"* ]]; then
	#		fileName="feuilles.json"
	#	elif [[ "$fileLink" == *"lieux_dits"* ]]; then
	#		fileName="lieux_dits.json"
	#	elif [[ "$fileLink" == *"parcelles"* ]]; then
	#		fileName="parcelles.json"
	#	elif [[ "$fileLink" == *"prefixes_sections"* ]]; then
	#		fileName="prefixes_sections.json"
	#	elif [[ "$fileLink" == *"sections"* ]]; then
	#		fileName="sections.json"
	#	elif [[ "$fileLink" == *"subdivisions_fiscales"* ]]; then
	#		fileName="subdivisions_fiscales.json"
	#	else
	#		fileName="$fileLink.json"
	#	fi
#
	#	if [ ! -d "$folder" ]; then
    #		mkdir -p $folder
	#	fi
#
	#	curl -s $url/$link/$sublink/$fileLink | gzip -d > $folder/$fileName
#
	#	progress=$(echo "$dptCount / 101 * 100" | bc -l)
#
	#	printf "Terminé %d\n" $progress
	#done
  done
done
