import { MapContainer, TileLayer, GeoJSON, LayersControl } from 'react-leaflet'

import parcelles from './assets/31484-parcelles.json'
import bati from './assets/31484-batiments.json'
import communes from './assets/communes.json'
import regions from './assets/regions.json'
import departements from './assets/departements.json'

import './App.css';
import { useEffect, useRef, useState } from 'react'

const center = [43.61929716198888, 1.4434618432873223];
const styleParcelles = {
	fillColor: 'white',
	weight: 2,
	opacity: 0.5,
	color: 'white',  //Outline color
	fillOpacity: 0
}

const styleBati = {
	fillColor: 'black',
	weight: 1,
	opacity: 0.5,
	color: 'black',  //Outline color
	fillOpacity: 0.1
}

const styleCommunes = {
	fillOpacity: 0,
	weight: 1,
	opacity: 1,
	color: 'red'
}

function onEachFeature(feature, layer) {
	layer.on('mouseover', e => {
		e.target.setStyle({
			weight: 3,
			fillOpacity: 0.4
		});
	});
	layer.on('mouseout', e => {
		e.target.setStyle(styleParcelles);
	});
	layer.on('click', e => {
		alert("Parcelle " + (e.target.feature.properties.prefixe === "000" ? "" : e.target.feature.properties.prefixe + " ") + e.target.feature.properties.section + " " + e.target.feature.properties.numero + "\nSuperficie : " + e.target.feature.properties.contenance + "m²");
	})
	layer.on('zoom', e => {
		console.log(e.target)
	})
}

function App() {

	let mapRef = useRef(null)
	useEffect(() => {
		setTimeout(() => {
			if (mapRef.current) {
				mapRef.current.on("zoomend", handleZoom);
			}
		}, 10);
	}, []);

	const [zoomLevel, setZoomLevel] = useState(5);

	const [showCommunes, setShowCommunes] = useState(false);
	const [showRegions, setShowRegions] = useState(true);
	const [showDpt, setShowDpt] = useState(false);

	const handleZoom = (e) => {
		setZoomLevel(e.target._zoom)
		if (e.target._zoom > 11) {
			setShowCommunes(true)
			setShowDpt(false)
			setShowRegions(false)
		} else if (e.target._zoom > 7) {
			setShowCommunes(false)
			setShowDpt(true)
			setShowRegions(false)
		} else if (e.target._zoom > 0) {
			setShowCommunes(false)
			setShowDpt(false)
			setShowRegions(true)
		}
	}

	return (
		<MapContainer preferCanvas={true} center={center} zoom={zoomLevel} style={{ width: '100vw', height: '100vh' }} maxZoom={30} ref={mapRef}>
			<TileLayer
				url='https://api.maptiler.com/maps/hybrid/256/{z}/{x}/{y}.jpg?key=fOfZYlU7d3vCKVbpPLoG'
				attribution='<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'>
			</TileLayer>

			<LayersControl position='topright'>
				<LayersControl.Overlay name="Parcelles">
					<GeoJSON key='parcelles' data={parcelles} style={styleParcelles} onEachFeature={onEachFeature} />
				</LayersControl.Overlay>
				<LayersControl.Overlay name="Bati">
					<GeoJSON key='bati' data={bati} style={styleBati} />
				</LayersControl.Overlay>
				<LayersControl.Overlay name="Communes" checked={showCommunes}>
					<GeoJSON key='communes' data={communes} style={styleCommunes} />
				</LayersControl.Overlay>
				<LayersControl.Overlay name="Régions" checked={showRegions}>
					<GeoJSON key='regions' data={regions} style={styleCommunes} />
				</LayersControl.Overlay>
				<LayersControl.Overlay name="Départements" checked={showDpt}>
					<GeoJSON key='dpt' data={departements} style={styleCommunes} />
				</LayersControl.Overlay>
			</LayersControl>
		</MapContainer>
	);
}

export default App; 
