lower_bound=03001
upper_bound=03321
dpt=03


#for i in $(seq $lower_bound $upper_bound); do
#    state=$(echo "($i - $lower_bound) / ($upper_bound - $lower_bound) * 100" | bc -l)
#    echo "$state%..."
#    wget -q -P ./src/assets/communes/$dpt/ https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/communes/$dpt/0$i/cadastre-0$i-communes.json.gz
#done

for subfolder in "./src/assets/communes"/*; do
    # check if subfolder is a directory
    if [ -d "$subfolder" ]; then

        subfolderName=$(basename $subfolder)

        url="https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/communes/$subfolderName/"

        # use curl to download the webpage
        nbCommunes=$(curl -s "$url" |
        # Use grep to filter the folders
        grep 'href' |

        # Use awk to extract the folder name
        awk -F '"' '{print $2}' |

        # get the last folder
        tail -1 |

        # remove the trailing '/'
        sed 's/\/$//')

        echo "$nbCommunes"

    fi
done